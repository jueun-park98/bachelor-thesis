# 1 Specification-based Approach

## 1.1 Kind of code coverage

- an operation is covered when at least one test case uses such operation
- a parameter is covered when at least one test case includes an operation targeting such parameter;
- an endpoint is covered when all its operations are covered
- a definition is covered when it is used in at least one test case

## 1.2 Concrete type of bugs/errors

- Nominal test case : Falls die test cases Antworten mit 4xx oder 500 codes bekommen -> Errors
- Faulty test case : Falls die test cases Antworten mit 2xx oder 500 codes bekommen -> Errors

## 1.3 How are test cases generated?

APIRequests beinhaltet operationId, schema (http, https) und Parameter

- Nominal test case definition : APIRequest generated with required parameter values
    - has 3 Assertions : ValidStatusCode, Schema (If the response ist 200 and schema of the response is in the definition), HeaderExists (If the response ist 200 and schema of the response is in the definition)

- Faulty test case definition
    - a test case has one of Required Parameter missing, Wrong data types or violated constraints
    - all test cases have 4xx status codes

- Inferring Parameter Values
  -A value of a parameter p could be inferred from: (1) examples (i.e.,p.example and p.schema.example), (2) default values (i.e., p.default or p.items.default if p of type array) or (3) enums (i.e., the first value of p.enum or
  p.items.enum if p is of type array)
    - A value of a parameter p could be set to a dummy value (respecting the type) if a request to the operation of p including that value returns a successful response (i.e., a 2xx response code class).
    - A value of a parameter p could be inferred from the response of an operation o if: (1) o is testable; (2) o returns a successful response r; and (3) r.schema contains a property matching p

# 2 Property-based Approach (QuickREST)

## 2.1 Kind of code coverage

only defend on the status codes in the response. i.e. : 201 + 400 + 500 = 100%

## 2.2 Concrete type of bugs/errors

only 500 status codes

## 2.3 How are test cases generated?

- with property-based testing : similar to the first approach, random test cases are generated in many iterations
- and schrinking : If a test case with a property fails, the library tries to find a smallest input in the property which causes the same error.

# 3 Search-based Approach (EvoMaster)

## 3.1 Kind of code coverage

statement (operation, get, post) coverage

## 3.2 Concrete type of bugs/errors

only 500 status codes

## 3.3 How are test cases generated?

- Test cases : HTTP-Requests with different types of parameters or complex objects
- the test suites are randomly generated like before and are used to be searched in Genetic Algorithm
- The fitness of a test suite is the aggregated fitness of all of its test cases. The crossover
  operator will mix test cases from two parent sets when new
  offspring are generated. The mutation operator will do small
  modifications on each test case, like increasing or decreasing
  a numeric variable by 1.
- if a test is executed, it will be saved in an archive. At the end of the search, all tests are removed except the minimised suite

# 4 Constraint-based Approach (RESTest)

## 4.1 Kind of code coverage

no coverage proposed

## 4.2 Concrete type of bugs/errors

1. The status code must be lower than 500 (server error).
2. The response must conform to the OAS schema.
3. If the request violates the specification of individual parameters (e.g., a mandatory parameter is missing), the status code must not be 2XX (successful response).
4. If the request violates one or more inter-parameter dependencies, the
   status code must not be 2XX.
5. If the request is valid according to the API specification, the status code must not be 4XX (client error response).

## 4.3 How are test cases generated?

1. Default Test Model Generation : OpenAPI -> in YAML with configuration settings
2. Automated Analysis of Inter-Parameter Dependencies
  - usage of IDLReasoner (IDLReasoner analyzes constraints of specification, CSP)
  -  This tool transforms the specification into a CSP and automatically checks for inconsistencies in the specification, informing the user about any errors, e.g., parameters that cannot be selected. Once the specification is validated, IDLReasoner provides test case generators with a catalogue
     of helpful analysis operations.
  - isValidRequest, getRandomValidRequest, getRandomInvalidRequest
3. Abstract Test Case Generation

# 5 Resource-based Approach

## 5.1 Kind of code coverage

statement (operation) coverage

## 5.2 Concrete type of bugs/errors

## 5.3 How are test cases generated?

# 6 Operation Dependency Graph Approach (RESTTESTGEN)

## 6.1 Kind of code coverage

operation coverage

## 6.2 Concrete type of bugs/errors

Nominal : 4xx, 5xx codes
Error : 2xx, 5xx codes

## 6.3 How are test cases generated?

ODG : when there exists a common field in the output (response) of n1 and in the input (request) of n2
If they are of atomic type (i.e., string or numeric) and have the same name;
If they are of non-atomic type (i.e., structured) and are associated to the same schema

Nominal Test Generator:
- usage of Response Dictionary ( For
  each operation that is successfully tested, the values of all
  the output fields that can be found in the response content
  are saved in the Response Dictionary )
- parameters are generated as Default and example values, values in Enum or Random Input
  Error Test Generator:
- usage of the results of Nominal Test Generator
- Mutation with following cases:
1. Missing required
2. Wrong input type
3. Constraint violation

# 7 Stateful Fuzzing Approach (RESTler)

## 7.1 Kind of code coverage

code lines

## 7.2 Concrete type of bugs/errors

only 500 status codes

## 7.3 How are test cases generated?

- generates requests with parameters which are fuzzed and saved in user-configurable dictionary
  - default values in integer : 0, 1, -1
  - default values in string : "sampleString", "", long fixed string 
