# Automatic Generation of Test Cases for REST APIs: a Specification-Based Approach

## General
- https://github.com/opendata-for-all/api-tester
- There are four steps to generation :
    1. parsing and processing OpenAPI model
    2. adding parameter examples
    3. generating a test suite model from the OpenAPI model by inferring test case definitions for the API operations
    4. transforming the test suite model into executable code. (JUnit in this paper)

## Specification-based API Testing
- verification of the API using the available functionalities defined in a specification document.
- test cases consist of sending requests over HTTP/S and validating them. ( through confirmation from server )
- automated specification-based API testing involves generating those test cases.
- fault-based test cases prove that known-faults do not exist. ( test cases with incorrect value )

## OpenAPI to test suite transformation
- There are two generation rules:
    1. Nominal test case definition ( correct input data )
    2. Faulty test case definition ( incorrect input data )
- test cases with nominal definitions have following assertions :
    1. ValidStatusCodeAssertion
    2. SchemaComplianceAssertion
    3. HeaderExistsAssertion
    - all these assertions return 2xx status codes.
- each test case with faulty definition is in one of following cases:
    1. Required missing
    2. Wrong data types
    3. Violated constraints
    - these cases return 4xx status codes.

## Code Generation
- test case definitions are platform-independent -> any programming language or testing tool can be used

## Images
1 <br> <img src="/images/AGOTC1.png"  width="60%" height="60%"> </br> <br> </br> 
2 <br> <img src="/images/AGOTC2.png"  width="60%" height="60%"> </br> <br> </br>
3 <br> <img src="/images/AGOTC3.png"  width="60%" height="60%"> </br> <br> </br> 
4 <br> <img src="/images/AGOTC4.png"  width="60%" height="60%"> </br> <br> </br>
5 <br> <img src="/images/AGOTC5.png"  width="60%" height="60%"> </br> <br> </br>
6 <br> <img src="/images/AGOTC6.png"  width="60%" height="60%"> </br> <br> </br>

# QuickREST: Property-based Test Generation of OpenAPI-Described RESTful APIs

## General
- uses OpenAPI documents, Clojure
- a method that for a given specification produces **input generators** that are used in property-based tests as well as produces **automatic oracles**.
- test inputs are generated in two-fold mechanism:
  1. randomly generated values that are **agnostic** to the specification
  2. randomly generated values that **conform** to the parameter specification in the given OpenAPI document
- test oracles assert the REST API results in a property-based way
- uses libraries in Clojure, to use the functionality to define specifications of data and to validate if given data conforms to such a specification (maybe exist also in Java?)
- proposes the **bidirectional use** of the specification. 
  - specification is used in generating **valid inputs** and **automated oracle**
- may give more information of system's behavior

## Property-based testing
- The main idea of property-based testing is to generate
  input data and to check if defined properties hold when exercising the system with that input
- if random data has been generated to which a property has failed, the library will try to find the smallest input that fails the property in the same way (shrinking)
- property
  1. predefined static properties (HTTP status codes)
  2. properties automatically derived as valid responses

## Implementation
1. Acquire the OpenAPI document, via HTTP or a file.
2. Parse the JSON document to an internal format. 
   1. Attach specifications for the parameters. 
   2. Attach specifications for the responses. 
3. Generate specifications for data definitions.
4. Make test generators based on the specifications.
5. Check the properties for each of the HTTP verbs defined.
   1. If in a stateful sequence of operations, store the
   response.
   2. Select next input from the collected responses. 
6. Report the test result.

# RESTful API Automated Test Case Generation
- generates integration tests
- in this paper, white-box testing, also available for black-box testing (https://github.com/EMResearch/EvoMaster/blob/master/docs/blackbox.md)
- two goals
  1. maximising code coverage (e.g., statement coverage)
  2. finding faults using the HTTP return statuses as an automated oracle
- uses Genetic Algorithm (Search Algorithm) to generate tests
- Test cases
  - one or more HTTP requests towards a RESTful Service
  - The test data can hence be seen as a string, representing the HTTP request
  - HTTP requests are based on OpenAPI documents, which describe what API methods are available
- Oracle
  - test cases for REST APIs can be used for regression testing
  - usage with HTTP status codes

## Implementation
<br> <img src="/images/Searchbased1.png"  width="60%" height="60%"> </br>

  
## Search-based testing
- Software testing is modeled as an optimization problem ( corresponding to goals above )
- fitness function is defined for a given testing problem
- search algorithm ( like GA ) can be used to explore the space of test cases
- GA
  - search individual is a set of test cases, randomly initialized, with variable size and length.
  - search individuals are selected for reproduction 
  - go through a crossover operator and mutations
  - The evolution ends either when:
    - an optimal individual is evolved
    - or the search has run out of the time

# RESTTESTGEN: Automated Black-Box Testing of RESTful APIs
- is based on the definition of REST API, including
  - the list of operations available
  - the format of the input/output data of their requests and responses
- uses Operation Dependency Graph
  - to model data dependencies among operations from REST API
  - is updated when automatically generating the test cases
- has two perspectives for test
  - nominal execution scenarios : valid input data in the document
  - error execution scenarios : defects or unhandled exceptions

## Implementation
<br> <img src="/images/RESTTESTGEN1.png"  width="60%" height="60%"> </br>

1. analyzes OpenAPI (Swagger) documents and computes the Operation Dependency Graph
2. reads OPG and the OpenAPI and generates nominal test cases
3. applies a catalog of mutation operators and stresses the data validation

# RESTest: Automated Black-Box Testing of RESTful Web APIs
- supports the specification and automated analysis of inter-parameter dependencies
- Constraint-based testing
- enables a better coverage with:
  1. systematic generation of valid and invalid combinations
  2. use of novel output assertions (i.e. test oracles)

## Implementation
<br> <img src="/images/RESTest1.png"  width="60%" height="60%"> </br>

### Default Test Model Generation
- takes as input the OpenAPI specification
- test model includes all test-related configuration settings for the API under test
- formatted in YAML (same language in OpenAPI specification) and includes additional settings :
  1. Operations under test
  2. Authentication data
  3. Test data generators (customizing data values)
  4. Weights ( in the range of [0, 1], higher weight = more frequently used )

### Automated Analysis of Inter-Parameter Dependencies
- input : OpenAPI, parameter dependencies in IDL
- transforms the specification into a CSP
- checks for inconsistencies in the specification, informing the
  user about any errors, e.g., parameters that cannot be selected
- if the specification is validated, IDLReasoner provides test case generators
- three analysis operations:
  1. isValidRequest
  2. getRandomValidRequest
  3. getRandomInvalidRequest

### Abstract Test Case Generation
- abstract test cases are platform-independent ( transformed later into executable test cases )
- random and constraint-based test case generation, but also other techniques are usable ( like search-based generation )
- comprises test inputs, expected outputs and the required information to build the API request ( endpoints )
- each test case performs a single API request
- generates nominal and faulty test cases
- test oracles : HTTP status codes

### Test Case Generation and Execution
- generates executable test cases with REST Assured (Java library for testing RESTful Service)
- test execution can be done offline or online
  - offline : test case generation and execution are independent
  - online : test case generation and execution are interleaved

# Resource-based Test Case Generation for RESTful Web Services
- extension of EvoMaster (Search-based Testing)
- uses MIO Algorithm ( maximizes code coverage and fault finding )
- Testing targets :
  1. coverage of statements
  2. coverage of branches
  3. returned HTTP status codes

## Resource Individuals
- a test case composed of a sequence of HTTP calls (specific HTTP method and an associated resource, defined by URI) 
- Search-based techniques use random sampling to create new individuals.
- uses templates to sample new individuals, instead of sampling them completely at random
- Templates
  <br> <img src="/images/REBATEST1.png"  width="60%" height="60%"> </br>
- four methods to select resources
  <br> <img src="/images/REBATEST1.png"  width="60%" height="60%"> </br>
- five sample strategies
  1. Equal-Probability : select methods at random with uniform probability, i.e., the probability for each applicable method is equal.
  2. Action-Based : the probability for each applicable method is derived based on a number of independent or not templates for all resources
  3. Used-Budget-Based : the probability for each applicable method is adaptive to the used budget (i.e., time or number of fitness evaluations) during search
  4. Archive-Based : the probability for each applicable method is adaptively determined by its performance during the search
  5. ConArchive-Based

## Resource-based Mutation
- improves the search
- mutates
  1. values on parameters
  2. structure of a test
- additionally, five operators are proposed
  1. DELETE : delete a resource together with all associated actions
  2. SWAP : swap the position of two resources together with all associated actions
  3. ADD: add a set of actions on a new resource path
  4. REPLACE: replace a set of actions on a resource with another set of actions on a new resource path
  5. MODIFY: modify a set of actions on a resource with another template

# RESTler: Stateful REST API Fuzzing
- performs a lightweight static analysis of Swagger(OpenAPI) specification.
- generates and executes tests
  1. inferring dependencies among request types declared in the Swagger specification
  2. analyzing dynamic feedback from responses observed

## Test Generation Algorithm
<br> <img src="/images/RESTler1.gif"  width="60%" height="60%"> </br>


# Summary

|  | Main Programming Language | Type of Bugs/Errors | Usage of OpenAPI | Research Goals | Found Bugs/Errors | Highest Code Coverage | Notes |   
|---|---|---|---|---|---|---|---|
| Specification-based Approach | Java | - Nominal Test Cases : 4xx/500 Status Codes, Schema Errors <br> - Faulty Test Cases : 500, 200 Status Codes | Yes | - High level coverage <br> - Finding main failing points  | 37 in the 91 selected APIs | - Operations : 87% <br> - Parameters : 62% <br> - Endpoints : 81% <br> - Definitions : 76% | |
| Property-based Approach (QuickREST) | Clojure | 500 Status Codes | Yes | - Behavior of several stateless generators <br> - Behavior of stateful generators and stateless generators <br> - Addition related to finding faults |  | - Response Code Coverage of POST Method <br> - 201 : 86.5% <br> - 400 : 83.6% <br> - 500 : 16.4% | - Found that several of the service APIs are under-specified |
| Search-based Approach (EvoMaster) | Kotlin | 5xx Status Codes | Yes | - Find real bugs <br> - Compare code coverage to manual tests <br> - Find main factors for better results | 39 real bugs | 41% (18%, 20%, 41%) | Main factors for better results : String constraints, accesses to databases, external web services |
| Constraint-based Approach (RESTest) | Java | - 5XX Status Codes <br> - OpenAPI Schema Errors <br> - 2XX Related to Parameters <br> - 2XX Related to Parameter Dependencies <br> - 4XX Status Codes | Yes | - Find out the effectiveness in generating valid test cases <br> - Find out the effectiveness in fault-finding to selected APIs | 1358 new bugs in comparison with random test cases ( 2920 -> 4278 ) | | |
| Resource-based Approach | Kotlin |   |   | - Find out that this approach gives improvement? <br> - Find out which of presented techniques gives best results? | | | - Improvement in 6 of 7 case studies <br> - MIO performs better |
| Operation Dependency Graph Approach (RESTTESTGEN) | Java | - Nominal Cases : 5xx Status Codes or Validation Error of Response <br> - Error Cases : 2xx or 5xx Status Codes | Yes | - Find out how effective the proposed Nominal Test Generator is <br> - Find out how effective the proposed Error Test Generator is | - Nominal Cases : 151 Faults, 1733 response validation errors <br> - Error Cases : 864 cases are accepted, 23 cases with unhandled errors |  | |
| Stateful Fuzzing Approach (RESTler) | Python |  | Yes | - Find out how effective proposed techniques are (inferring dependencies among request types, analyzing dynamic feedback) <br> - Find out if RESTler exercises deeper as sequence length increases <br> - Compare three proposed search strategies in terms of code coverage | 28 bugs in GitLab, several bugs in Azure, Office 365 cloud service | no percentages or total number given |  |