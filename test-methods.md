# RESTler

## General
- stateful REST API fuzzing tool for automatically testing
- uses and analyzes OpenAPI specification
- generates and executes tests that exercise the service through its REST API
- infers producer/consumer dependencies among request types from OpenAPI
- checks for specific classes of bugs
- learns service behavior from prior service responses
- finds deeper reachable service states out

## Main Algorithm
- parses OpenAPI specification in other way
  - sorts all values in static and fuzzable values

## Results

# Generation of Test Cases Approach Specification-based API Testing

## General
- https://github.com/opendata-for-all/api-tester
- test cases has on average 76.5% coverage of the elements of OpenAPI definitions
- 40%  of the tested APIs failed
- There are four steps to this way :
  1. parsing and processing OpenAPI model
  2. adding parameter examples
  3. generating a test suite model from the OpenAPI model by inferring test case definitions for the API operations
  4. transforming the test suite model into executable code. (JUnit in this paper)

## Specification-based API Testing
- verification of the API using the available functionalities defined in a specification document.
- test cases consist of sending requests over HTTP/S and validating them. ( through confirmation from server )
- automated specification-based API testing involves generating those test cases.
- fault-based test cases prove that known-faults do not exist. ( test cases with incorrect value )

## OpenAPI to test suite transformation
- There are two generation rules:
  1. Nominal test case definition ( correct input data )
  2. Faulty test case definition ( incorrect input data )
- test cases with nominal definitions have following assertions :
  1. ValidStatusCodeAssertion
  2. SchemaComplianceAssertion
  3. HeaderExistsAssertion
  - all these assertions return 2xx status codes.
- each test case with faulty definition is in one of following cases:
  1. Required missing
  2. Wrong data types
  3. Violated constraints
  - these cases return 4xx status codes.

## Code Generation
- test case definitions are platform-independent -> any programming language or testing tool can be used

## Research Questions / Results
1. What is the coverage level of the generated test cases?
   - 958 test cases ( 445 nominal / 513 faulty )
<br> <img src="/images/AGOTC_RQ1.png"  width="60%" height="60%"> </br>
2. What are the main failing points in the definitions and implementation of REST APIs?
   - 37 of 91 selected API made errors
<br> <img src="/images/AGOTC_RQ2.png"  width="60%" height="60%"> </br>

# Property-based Test Generation of OpenAPI-Described RESTful APIs

## General
- implemented in Clojure
- producing generation of input data -> property check.
- properties are in two types : static properties, properties automatically derived as valid responses.
- static properties are provided as HTTP status codes.
- valid responses are specified in the OpenAPI document.
- black-box testing
- bidirectional use of the specification -> a specification is used both to generate valid input as well as to generate a verdict in the form of an automated oracle.
- the oracle is used to validate that responses conform to the given specification.
- tests and oracles will automatically evolve with any changes to the API and its specification

## Property-based testing
- generate input data and check its defined properties hold when exercising the SUT (system under test) with that input.
- if a failed property follows from a generated random data, this test program will try to find the smallest input that fails in the same way. ( challenge )
- this challenge can be avoided by formulating invariants or symmetries instead of implementing the model.
- example of invariants : input length = output lengtht
- example of symmetry : compression algorithm returns the same data if it compress and decompress the data.
- first tool for property-based testing : QuickCheck

## Implementation
1) Acquire the OpenAPI document, via HTTP or a file.
2) Parse the JSON document to an internal format. 
   - Attach specifications for the parameters.
   - Attach specifications for the responses.
3) Generate specifications for data definitions.
4) Make test generators based on the specifications.
5) Check the properties for each of the HTTP verbs defined.
   -  If in a stateful sequence of operations, store the response.
   -  Select next input from the collected responses.
6) Report the test result.
   Figure 2 is an example of how a REST API, as described
   in Figure 1, would be processed and tested.

### Parse the JSON document
- input : OpenAPI document in JSON
- output : parsed document in a format suitable for further processing ( contains basic value types and reference types for parameters and responses )

### Generate specifications for the definitions
- Figure 1 to Figure 5

### Make test generators
- request generator : example in Figure 6
- URL generator : iterates over each parameter and insert the generated value in the URL ( Path, Query ) or as attached data ( Header, Body, Form )
- it can generate data defined outside the specification
  - in OpenAPI, some of parameters are required
  - in the generator, they might not be included.
- this allows the generators to produce test cases with some missing input or values out of range.
- result : valid URLs to call the API with random input

### Check properties

# REST API Automated Test Case Generation

## General
- white box testing
- EvoMaster, open-source : https://github.com/EMResearch/EvoMaster
- regression testing
- requires some manual configuration for REST API testing (Fig. 4)
- Fig. 4 uses Spring
- Search Algorithm : Genetic Algorithm

## Genetic Algorithm
- final output of the tool : test suite, which is a collection of test cases
- Each test case will cover one or more testing targets
- test targets :
1. coverage of statements in the system
2. returned HTTP status codes for the different API endpoints

# Automated API Testing

This paper introduces a tool to generate tests, but does not have a concrete link.
## Generated test cases
- **Input validation** - Test the API with different input parameters and verify the response. The data, response code, message etc should be correct for each set of input parameters. Does the API return correct HTTP error codes like 200 for valid and 400 for invalid input parameters?
- **JSON format validation** - Verify that the JSON or XML response of the API is correctly structured.
- **Business Logic** - If API returns the correct HTTP response code, but the actual balance being returned for user is of previous month, the business logic of this API is broken.
- **Negative test cases** - Hit the API with incorrect/invalid parameters, missing/extra values, null values for mandatory fields and observe the output.
- **Reliability tests** - Check whether the API can consistently return correct response, or do response failures occur often.
- **Call sequencing checks** - If the output of the API being tested includes modification of a data structure, change of a resource state (Like a DB record), firing of an event or call to other APIs, this should be functioning correctly.
- **Security testing** - to make sure that its code and services cannot be accessed and utilized by unapproved clients. 

# RESTTESTGEN: Automated Black-Box Testing of RESTful APIs

# RESTest: Automated Black-Box Testing of RESTful Web APIs

# Resource-based Test Case Generation for RESTful Web Services
